/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   twentyonesh.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 20:43:47 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 18:09:25 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TWENTYONESH_H
# define TWENTYONESH_H

# include "libft.h"
# include "line_edition.h"

# include <unistd.h>
# include <sys/types.h>
# include <string.h>
# include <signal.h>
# include <errno.h>

# include <term.h>
# include <termios.h>
# include <sys/ioctl.h>
# include <fcntl.h>

# define TO_EXIT 2
# define TO_ERROR 3
# define TO_PROMPT "$> "
# define TO_2001 "21sh: unsetenv: I'm afraid I cannot let you do that"

# define TO_UK -1
# define TO_WORD 0
# define TO_OP 1
# define TO_CMD 2
# define TO_ARG 3
# define TO_GRP 4
# define TO_PIPE 5
# define TO_FILE 6
# define TO_REDIR 7

# define TO_GREAT 8
# define TO_LESS 9
# define TO_DGREAT 10
# define TO_DLESS 11
# define TO_LESS_AND 12
# define TO_GREAT_AND 13
# define TO_AND_LESS 14
# define TO_AND_GREAT 15
# define TO_CLOSE_REDIR 16

# define TO_SEMICOLON 17

# define TO_QUOTE 18

# define TO_READ 1
# define TO_WRITE 2
# define TO_WRITE_PARENT 4
# define TO_DONT_FORK 8

typedef struct	s_op
{
	int		kind;
	t_token	*op1;
	t_token	*op2;
}				t_op;

/*
** main_loop.c
*/
int				loop(char ***env);

/*
** init.c
*/
char			**init(char **envp);

/*
** manage_term.c
*/
int				init_caps(t_caps *caps);
struct termios	*save_orig_termios(struct termios *orig_termios);
t_caps			*save_caps(t_caps *caps);
int				save_fd(int fd);
int				get_winsize(int *width, int *height);

/*
** manage_term2.c
*/
void			configure_term(void);
void			restore_term(void);
void			exit_term();

/*
** sighandlers.c
*/
void			register_signals(void);

/*
** eval.c
*/
int				fork_and_eval(t_bst *node, char ***env, int flags);
int				eval_token(t_token *t, char ***env);

/*
** runnners.c
*/
int				test_access(char *path);
char			*bin_path(char **env, char *bin);
int				run_builtin(t_token *t, char ***env);
int				run_bin(char *bin, char **argv, char **env);

/*
** manage_redirs.c
*/
void			manage_redirs(t_slist *redirs);
int				close_redirs(t_slist *redirs);

/*
** manage_redirs2.c
*/
void			for_less(t_redir *redir);

/*
** manage_pipe.c
*/
int				read_pipe(int *fd);
int				write_pipe(int *fd);
int				close_pipe(int *fd);
int				close_pipes(t_bst *ast);
void			manage_pipe(t_bst *ast, char ***env, int flags);

/*
** interpreter.c
*/
int				run(t_bst *ast, char ***env, int rw);
int				interprete(t_bst *ast, char ***env);

/*
** lexer.c
*/
t_tlist			*lexe(char *input, size_t len);

/*
** lexer_identify_redir.c
*/
int				*end_of_redir(char *s, int start);

/*
** lexer_make_tgs.c
*/
void			make_token_groups(t_tlist **tokens);

/*
** lexer_make_redir.c
*/
int				make_redirections(t_tlist **tokens, char *input);

/*
** lexer_make_heredoc.c
*/
void			make_heredoc_redir(t_redir *r, char *input);
void			make_heredoc(t_tlist **tokens, t_tlist *tl, t_redir *r);

/*
** parser.c
*/
t_tlist			*find_after_pipes(t_tlist *node);
t_bst			*make_ast(t_tlist *tl);
t_bst			*parse(t_tlist *tokens);

/*
** parser2.c
*/
t_tlist			*find_next_op(t_tlist *tl, int *ops);
t_tlist			*find_last_pipe(t_tlist *tl);
t_tlist			*find_prev_pipe(t_tlist *tl);
void			make_normal_node(t_bst *node, t_tlist *tl);

/*
** tokens_checker.c
*/
int				tokens_valid(t_tlist *tl);

/*
** token_utils.c
*/
int				is_cpartofw(char c);
int				next_nonblank(char *s, int start);
char			*kind_tostr(int kind);
int				kind_isop(int kind);
int				kind_isredir(int kind);

/*
** token_utils.c
*/
size_t			ntarr_len(t_token **tokens);

/*
** update_input.c
*/
void			update_input(char **input, char **env);
void			update_single_input(char *input, char **env);

/*
** builtins.c
*/
void			run_echo(char **input);
void			run_env(char **env);
void			run_history(char **input);

/*
** builtins_un_setenv.c
*/
int				run_setenv(char **input, char ***env);
int				run_unsetenv(char **input, char ***env);

/*
** builtins_cd.c
*/
int				run_cd(char **input, char ***env);

/*
** env.c
*/
char			**get_env(char **env, char *name);
char			*get_val(char **env, char *name);
int				set_val(char ***env, char *name, char *val);
int				add_room_env(char ***env);

/*
** utils.c
*/
int				check_unsetenv(char **input, char **env);
int				is_envname_equal(char *input, char *full);
char			*save_pwd(char *pwd);
void			new_path(char **path, char **to_split);
void			set_path(char **path, char *to);

#endif
