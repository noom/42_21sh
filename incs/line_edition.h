/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_edition.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 16:34:27 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:12:21 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LINE_EDITION_H
# define LINE_EDITION_H

# define NO_COLOR			"\033[0m"
# define SELECTION_COLOR	"\033[30;42m"
# define PROMPT_COLOR		"\033[33;1m"
# define QUOTE_COLOR		"\033[36;1m"
# define CHECK_COLOR		"\033[30;41m"
# define CURSOR_COLOR		"\033[30;5;47m"

# define TO_K_UP			"\e[A"
# define TO_K_DOWN			"\e[B"
# define TO_K_RIGHT			"\e[C"
# define TO_K_LEFT			"\e[D"

# define TO_K_END			"\e[F"
# define TO_K_HOME			"\e[H"

# define TO_K_DEL_FWD		"\e[3~"
# define TO_READ_SIZE		7
# define TO_K_ESC			'\e'
# define TO_K_DEL			127
# define TO_K_BAC			126
# define TO_K_RET			10
# define TO_K_SPA			' '

# define TO_K_CTRL_D		4
# define TO_K_CTRL_P		16
# define TO_K_CTRL_K		11
# define TO_K_CTRL_L		12
# define TO_K_CTRL_U		21
# define TO_K_CTRL_W		23

# define TO_K_SHIFT_UP		"\e[1;2A"
# define TO_K_SHIFT_DOWN	"\e[1;2B"
# define TO_K_SHIFT_LEFT	"\e[1;2D"
# define TO_K_SHIFT_RIGHT	"\e[1;2C"

# define TO_K_OPT_LEFT		"\e\e[D"
# define TO_K_OPT_RIGHT		"\e\e[C"

# define TO_K_CTRL_UP		"\e[1;5A"
# define TO_K_CTRL_DOWN		"\e[1;5B"

# define LEFT				0
# define RIGHT				1

# define STANDARD			0
# define QUOTE				1
# define DQUOTE				2
# define HEREDOC			3

# define HISTORY_MAX		200
# define HISTORY_LINE_SIZE	2000000

typedef struct			s_caps
{
	char				*up;
	char				*cr;
	char				*cd;
	char				*clr;
	char				*gto;
	char				*sta;
	char				*ste;
	char				*hic;
	char				*shc;
	char				*sav;
	char				*res;
	char				*ula;
	char				*ule;
	char				*lft;
	char				*rgt;
	char				*ins;
	char				*qin;
	char				*cin;
}						t_caps;

typedef struct			s_line
{
	char				data;
	char				cursor;
	char				selected;
	char				ignored;
	char				line_type;
	char				line_level;
	struct s_line		*next_line;
	struct s_line		*previous;
	struct s_line		*next;
}						t_line;

typedef struct			s_history
{
	t_line				*cmd;
	int					rank;
	char				top_upped;
	struct s_history	*previous;
	struct s_history	*next;
}						t_history;

typedef struct			s_edit
{
	t_line				*cursor;
	t_line				*sel_origin;
	t_line				*first;
	t_line				*head_list;
	t_line				*selected;
	t_line				*heredoc;
	char				sel_direction;
	char				mode;
	t_history			*history;
	char				has_returned;
}						t_edit;

t_edit					*g_edit;

/*
** cursor_tools.c
*/
void					check_cursor(void);
void					move_cursor_to_tail(void);
void					reset_cursor(void);
void					update_cursor_selection(void);

/*
** edit_cut.c
*/
void					copy_selection(void);
void					cut_left(void);
void					cut_right(void);
void					paste_selection(void);

/*
** edit_debug.c
*/
void					ft_putstr_debug(char *str);
void					print_vitefait(t_line *line);

/*
** edit_line.c
*/
int						ret_moves(void);
void					cut_moves(char *c);

/*
** edit_line_2.c
*/
char					*edit_loop();
int						event(char *c, t_caps *caps);

/*
** edit_print.c
*/
void					print_char(t_line *line);
void					print_cursor(char c);
void					print_history(unsigned char rank);
void					print_line(void);
void					print_selection(char c);

/*
** edit_tools.c
*/
int						ft_is_printable(int c);
int						get_line_number(void);
int						get_width(void);
void					init_edit(void);

/*
** free_tools.c
*/
void					free_heredoc(void);
void					free_history(void);
void					free_line(void);
void					free_maillon(t_line *line);
void					free_when_exit(void);

/*
** heredoc.c
*/
int						get_stopper(t_line *line);
int						compare_to_stopper(void);
t_line					*go_to_less_than(void);

/*
** history.c
*/
void					add_to_history(void);
t_history				*create_history(void);
void					reset_history(void);

/*
** history_move.c
*/
void					arrow_up(void);
void					arrow_down(void);
t_line					*copy_history(t_line *hist);
t_line					*make_new_line(void);

/*
** history_tools.c
*/
void					adjust_new_line(t_line *line);
void					free_current_level(void);
int						get_whole_length(void);
void					make_clean_line(void);
void					print_history_line(t_history *history);

/*
** line_basics.c
*/
void					add_to_line(char c);
char					check_closure(t_line *line);
t_line					*create_line(char c);
void					del_last_char(void);
t_line					*new_line(void);

/*
** line_delete.c
*/
void					del_next_char(void);
void					refresh_screen(void);
int						exit_and_submit(void);
int						exit_without_submit(void);
int						get_conversion_length(void);

/*
** line_tools.c
*/
void					clear_current_line(void);
int						combo_ctrl_d(void);
char					*convert_line(void);
void					del_line(void);
void					init_line(void);

/*
** line_update.c
*/
int						get_current_length(void);
int						get_nblen(int n);
void					update_first(void);
void					update_selected(void);

/*
** move.c
*/
void					move_down(void);
void					move_left(void);
void					move_right(void);
void					move_up(void);

/*
** move_by_word.c
*/
void					go_to_head(void);
void					go_to_next_word(void);
void					go_to_previous_word(void);
void					go_to_tail(void);

/*
** print_tools.c
*/
void					print_correct_prompt(t_line *line);
void					print_line_cursorless(void);
void					print_quote_prompt(int c);

/*
** quotes.c
*/
int						add_return_to_line(int quote, int ret);
int						eval_line(void);

/*
** selection.c
*/
void					delete_selection(void);
t_line					*go_to_first_selected(void);
t_line					*go_to_last_selected(void);
t_line					*make_twin(t_line *line);

/*
** selection_tools.c
*/
int						enter_selection_mode(void);
int						exit_selection_mode(void);
void					free_selection(void);
void					undo_selection(void);

#endif
