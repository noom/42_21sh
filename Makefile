# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/30 11:15:13 by echojnow          #+#    #+#              #
#    Updated: 2018/07/30 17:39:57 by noom             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = 21sh

CC = clang
FLAG = -g -Wall -Wextra #-Werror

INC_DIR = incs
SRCS_DIR = srcs
OBJS_DIR = objs

SRCS_NAME = \
	  main_loop.c \
	  twentyonesh.c \
	  edit_line.c \
	  line_basics.c \
	  manage_term.c \
	  manage_term2.c \
	  sighandlers.c \
	  init.c \
	  runners.c \
	  eval.c \
	  manage_redirs.c \
	  manage_redirs2.c \
	  manage_pipe.c \
	  interpreter.c \
	  lexer.c \
	  lexer_identify_redir.c \
	  lexer_make_tgs.c \
	  lexer_make_redirs.c \
	  lexer_make_heredoc.c \
	  tokens_checker.c \
	  parser.c \
	  parser2.c \
	  token_utils.c \
	  token_utils2.c \
	  update_input.c \
	  builtins.c \
	  builtins_un_setenv.c \
	  builtin_cd.c \
	  env.c \
	  utils.c \
	  move.c \
	  move_by_word.c \
	  line_update.c \
	  line_tools.c \
	  line_delete.c \
	  selection.c \
	  edit_tools.c \
	  cursor_tools.c \
	  selection_tools.c \
	  quotes.c \
	  history.c \
	  history_move.c \
	  history_tools.c \
	  edit_print.c \
	  print_tools.c \
	  edit_cut.c \
	  heredoc.c \
	  edit_debug.c \
	  free_tools.c \
	  edit_line_2.c \

OBJS_NAME = $(SRCS_NAME:.c=.o)

SRCS = $(addprefix $(SRCS_DIR)/, $(SRCS_NAME))
OBJS = $(addprefix $(OBJS_DIR)/, $(OBJS_NAME))

INCLUDE = -I$(INC_DIR) -Ilibft/includes
LIB = -Llibft -lft -lncurses

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(FLAG) $^ -o $@ $(LIB)

$(OBJS_DIR)/%.o: $(SRCS_DIR)/%.c
	@mkdir -p $(OBJS_DIR)
	$(CC) $(FLAG) $(INCLUDE) -c $^ -o $@

clean:
	$(RM) -r $(OBJS_DIR)

fclean: clean
	$(RM) $(NAME)

re: fclean all

run: all
	./$(NAME)

rund: all
	./$(NAME) 2&>/dev/null

deb: all
	@valgrind --track-origins=yes ./$(NAME)

debl: all
	@valgrind --track-origins=yes --leak-check=full ./$(NAME)

norm:
	@norminette $(SRCS) $(INC_DIR)/twentyonesh.h | grep -B1 "Warning\|Error" \
		|| echo OK

vim:
	@nvim Makefile $(INC_DIR)/twentyonesh.h $(SRCS)
