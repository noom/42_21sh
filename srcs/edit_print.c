/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   edit_print.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/25 12:03:26 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:14:02 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void			print_line(void)
{
	t_line		*line;

	line = g_edit->first;
	print_correct_prompt(line);
	while (line != NULL)
	{
		print_char(line);
		if (line->data == '\n' && line->next != NULL
				&& line->line_type != line->next->line_type)
			print_quote_prompt(line->line_type);
		line = line->next;
	}
}

void			print_cursor(char c)
{
	ft_putstr(CURSOR_COLOR);
	ft_putchar(c);
	ft_putstr(NO_COLOR);
}

void			print_selection(char c)
{
	ft_putstr(SELECTION_COLOR);
	ft_putchar(c);
	ft_putstr(NO_COLOR);
}

void			print_history(unsigned char rank)
{
	t_history	*history;

	history = g_edit->history;
	if (!history || rank > HISTORY_MAX)
		return ;
	while (history->previous)
		history = history->previous;
	if (rank == 0)
		ft_putendl("\n-- HISTORY --");
	while (history)
	{
		if (rank == 0)
			print_history_line(history);
		else if (history->rank == rank)
		{
			print_history_line(history);
			return ;
		}
		history = history->next;
	}
}

void			print_char(t_line *line)
{
	if (g_edit->mode == 0)
	{
		if (line->cursor == 0)
			ft_putchar(line->data);
		else
			print_cursor(line->data);
	}
	else
	{
		if (line == g_edit->sel_origin)
			print_selection(line->data);
		else if (line->selected == 0)
			ft_putchar(line->data);
		else
			print_cursor(line->data);
	}
}
