/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 19:36:33 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 16:18:13 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

t_tlist	*find_next_op(t_tlist *tl, int *ops)
{
	int	kind;
	int	i;

	while (tl)
	{
		kind = tl->t->kind;
		i = -1;
		while (++i < 2)
		{
			if (ops[i] == kind)
				return (tl);
		}
		tl = tl->next;
	}
	return (NULL);
}

t_tlist	*find_last_pipe(t_tlist *tl)
{
	t_tlist	*pipe;

	while (tl && tl->t->kind != TO_SEMICOLON)
	{
		if (tl->t->kind == TO_PIPE)
			pipe = tl;
		tl = tl->next;
	}
	return (pipe);
}

t_tlist	*find_prev_pipe(t_tlist *tl)
{
	tl = tl->prev;
	while (tl)
	{
		if (tl->t->kind == TO_PIPE)
			return (tl);
		if (tl->t->kind == TO_SEMICOLON)
			return (NULL);
		tl = tl->prev;
	}
	return (NULL);
}

void	make_normal_node(t_bst *node, t_tlist *tl)
{
	t_bst	*iter;

	iter = node;
	while (iter && iter->right)
		iter = iter->right;
	iter->right = make_ast(find_after_pipes(tl));
	if (iter->right)
		iter->right->parent = iter;
}
