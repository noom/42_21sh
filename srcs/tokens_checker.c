/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokens_checker.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/24 19:48:28 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/07/31 14:04:19 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

static int	pipe_valid(t_tlist *tl)
{
	if (!(tl->prev && tl->next))
		return (0);
	if ((tl->prev->t->kind != TO_CMD && tl->prev->t->kind != TO_GRP)
			|| (tl->next->t->kind != TO_CMD && tl->next->t->kind != TO_GRP))
		return (0);
	return (1);
}

static int	redirs_valid(t_tlist *tl)
{
	t_slist	*iter;
	t_redir	*r;

	iter = tl->t->redirs;
	while (iter)
	{
		r = (t_redir*)iter->data;
		if (!kind_isredir(r->kind))
			return (0);
		if (r->kind != TO_GREAT_AND && r->kind != TO_CLOSE_REDIR
				&& r->kind != TO_DLESS && !r->file)
			return (0);
		iter = iter->next;
	}
	return (1);
}

int			tokens_valid(t_tlist *tl)
{
	t_tlist	*iter;

	iter = tl;
	while (iter)
	{
		if ((iter->t->kind == TO_PIPE && !pipe_valid(iter))
				|| (iter->t->redirs && !redirs_valid(iter))
				|| (kind_isredir(iter->t->kind)))
			return (0);
		iter = iter->next;
	}
	return (1);
}
