/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   edit_line_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 16:34:27 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:12:21 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

int						event(char *c, t_caps *caps)
{
	FT_UNUSED(caps);
	cut_moves(c);
	if (c[0] == TO_K_CTRL_D && c[1] == 0)
		return (combo_ctrl_d());
	else if (c[0] == TO_K_RET && g_edit->mode == 0)
	{
		if (get_whole_length() > 1)
			return (ret_moves());
		else
		{
			refresh_screen();
			g_edit->cursor->cursor = 0;
			print_line();
			ft_putchar('\n');
			g_edit->cursor->cursor = 1;
			print_line();
		}
	}
	return (1);
}

char					*edit_loop(void)
{
	int					running;
	char				c[7];
	t_caps				*caps;

	running = 1;
	ft_memset(&c, 0, TO_READ_SIZE);
	caps = save_caps(NULL);
	print_line();
	while (running)
	{
		read(0, &c, TO_READ_SIZE);
		running = event(&c[0], caps);
		if (running == 2)
			return ("exit");
		ft_memset(&c, 0, TO_READ_SIZE);
	}
	return (convert_line());
}
