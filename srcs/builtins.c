/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 14:45:34 by echojnow          #+#    #+#             */
/*   Updated: 2018/04/26 18:50:43 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void		run_echo(char **input)
{
	int	i;

	i = -1;
	while (input[++i])
	{
		ft_putstr(input[i]);
		if (input[i + 1])
			ft_putchar(' ');
	}
	ft_putchar('\n');
}

void		run_env(char **env)
{
	while (*env)
		ft_putendl(*(env++));
}

void		run_history(char **input)
{
	int	rank;

	if (!input)
		rank = 0;
	else
		rank = ft_atoi(input[0]);
	if (rank >= 0 && rank < HISTORY_MAX)
		print_history(rank);
}
