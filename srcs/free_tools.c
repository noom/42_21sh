/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/24 19:48:28 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:17:24 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void				free_when_exit(void)
{
	free_history();
	free_selection();
	free(g_edit);
}

void				free_heredoc(void)
{
	t_line			*damned;
	t_line			*next;

	if (g_edit->heredoc == NULL)
		return ;
	damned = g_edit->heredoc;
	next = damned;
	while (damned->next != NULL)
	{
		next = damned->next;
		free(damned);
		damned = next;
	}
	free(damned);
	g_edit->heredoc = NULL;
}

void				free_line(void)
{
	t_line			*damned;
	t_line			*next;

	check_cursor();
	if (g_edit->head_list == NULL)
		return ;
	damned = g_edit->head_list;
	next = damned;
	while (damned->next != NULL)
	{
		next = damned->next;
		free(damned);
		damned = next;
	}
	free(damned);
	g_edit->head_list = NULL;
	g_edit->first = NULL;
	g_edit->cursor = NULL;
}

void				free_maillon(t_line *line)
{
	t_line			*next;

	next = NULL;
	while (line != NULL)
	{
		next = line->next;
		free(line);
		line = next;
	}
}

void				free_history(void)
{
	t_history		*damned;
	t_history		*previous;

	if (g_edit->history == NULL)
		return ;
	reset_history();
	damned = g_edit->history;
	previous = damned;
	while (damned != NULL)
	{
		previous = damned->previous;
		free_maillon(damned->cmd);
		free(damned);
		damned = previous;
	}
	g_edit->history = NULL;
}
