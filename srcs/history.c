/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 16:44:37 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/22 13:57:42 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

t_history				*create_history(void)
{
	t_history			*hist;

	if ((hist = (t_history *)ft_memalloc(sizeof(t_history))) == NULL)
		return (NULL);
	hist->cmd = g_edit->head_list;
	hist->rank = 1;
	hist->top_upped = 0;
	hist->previous = NULL;
	hist->next = NULL;
	return (hist);
}

void					reset_history(void)
{
	t_history			*hist;

	if (g_edit->history == NULL)
		return ;
	if (g_edit->history->next != NULL)
	{
		hist = g_edit->history;
		while (hist->next != NULL)
			hist = hist->next;
		g_edit->history = hist;
		hist->top_upped = 0;
	}
}

static void				update_history_ranks(void)
{
	t_history			*hist;

	hist = NULL;
	if (g_edit->history->rank > HISTORY_MAX)
	{
		hist = g_edit->history;
		while (hist->previous != NULL && hist->rank > 1)
		{
			hist->rank--;
			hist = hist->previous;
		}
		hist->next->previous = NULL;
		free_maillon(hist->cmd);
		free(hist);
	}
}

void					add_to_history(void)
{
	t_history			*hist;

	if (get_whole_length() <= 1 || get_whole_length() > HISTORY_LINE_SIZE)
		return ;
	reset_history();
	make_clean_line();
	if ((hist = create_history()) == NULL)
		return ;
	if (g_edit->history == NULL)
		g_edit->history = hist;
	else
	{
		g_edit->history->next = hist;
		hist->previous = g_edit->history;
		hist->rank = g_edit->history->rank + 1;
		g_edit->history = hist;
		update_history_ranks();
	}
}
