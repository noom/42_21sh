/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_by_word.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 13:09:53 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/24 21:23:30 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void			go_to_head(void)
{
	check_cursor();
	g_edit->cursor->cursor = 0;
	g_edit->first->cursor = 1;
	reset_cursor();
	refresh_screen();
	print_line();
}

void			go_to_tail(void)
{
	t_line		*tmp;

	check_cursor();
	if (g_edit->cursor->next == NULL)
		return ;
	tmp = g_edit->cursor;
	while (tmp->next != NULL)
		tmp = tmp->next;
	g_edit->cursor->cursor = 0;
	tmp->cursor = 1;
	reset_cursor();
	refresh_screen();
	print_line();
}

static int		zap_char(char c)
{
	if (ft_isblank(c) || c == '\n')
		return (1);
	return (0);
}

void			go_to_next_word(void)
{
	t_line		*tmp;
	t_line		*tmp1;

	check_cursor();
	if (g_edit->cursor->next == NULL)
		return ;
	tmp = g_edit->cursor;
	tmp1 = tmp;
	while (tmp->next != NULL && !zap_char(tmp->next->data))
		tmp = tmp->next;
	while (tmp->next != NULL && zap_char(tmp->next->data))
		tmp = tmp->next;
	g_edit->cursor->cursor = 0;
	if (tmp->next != NULL)
		tmp->next->cursor = 1;
	else
		tmp1->cursor = 1;
	reset_cursor();
	refresh_screen();
	print_line();
}

void			go_to_previous_word(void)
{
	t_line		*tmp;

	check_cursor();
	if (g_edit->cursor->previous == NULL)
		return ;
	tmp = g_edit->cursor;
	while (tmp->previous != NULL && zap_char(tmp->previous->data)
		&& tmp->previous->line_level == g_edit->cursor->line_level)
		tmp = tmp->previous;
	while (tmp->previous != NULL && !zap_char(tmp->previous->data)
		&& tmp->previous->line_level == g_edit->cursor->line_level)
		tmp = tmp->previous;
	if (!zap_char(tmp->data))
	{
		g_edit->cursor->cursor = 0;
		tmp->cursor = 1;
	}
	reset_cursor();
	refresh_screen();
	print_line();
}
