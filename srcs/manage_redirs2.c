/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_redirs2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 14:05:50 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 17:28:37 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void	for_less(t_redir *redir)
{
	redir->fd = open(redir->file, O_CREAT | O_RDONLY, 0644);
	dup2(redir->fd, STDIN_FILENO);
}
