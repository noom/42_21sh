/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   interpreter.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: noom <echojnow@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/25 18:55:48 by noom              #+#    #+#             */
/*   Updated: 2018/07/19 19:19:58 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

int		run(t_bst *ast, char ***env, int flags)
{
	int	kind;

	if (!ast)
		return (0);
	if (run(ast->left, env, flags))
		return (-1);
	kind = ((t_tlist*)ast->data)->t->kind;
	if (kind == TO_PIPE && !(ast->parent
				&& ((t_tlist*)ast->parent->data)->t->kind == TO_PIPE))
		manage_pipe(ast, env, flags);
	else if ((kind == TO_GRP || kind == TO_CMD) && !(ast->parent
				&& ((t_tlist*)ast->parent->data)->t->kind == TO_PIPE))
	{
		if (!ast->parent && !ast->left && !ast->right
				&& (!ft_strcmp(((t_tlist*)ast->data)->t->value, "cd")
					|| !ft_strcmp(((t_tlist*)ast->data)->t->value, "setenv")
					|| !ft_strcmp(((t_tlist*)ast->data)->t->value, "unsetenv")))
			eval_token(((t_tlist*)ast->data)->t, env);
		else if (fork_and_eval(ast, env, flags))
			return (-1);
	}
	if (run(ast->right, env, flags))
		return (-1);
	return (0);
}

int		interprete(t_bst *ast, char ***env)
{
	if (run(ast, env, 0))
		return (-1);
	return (0);
}
