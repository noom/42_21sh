/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   edit_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/24 19:48:28 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:17:24 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

int				ft_is_printable(int c)
{
	if (c >= 32 && c <= 126)
		return (1);
	return (0);
}

void			init_edit(void)
{
	t_edit		*edit;

	if ((edit = (t_edit *)malloc(sizeof(t_edit))) == NULL)
		return ;
	edit->first = new_line();
	edit->cursor = edit->first;
	edit->head_list = edit->first;
	edit->sel_origin = NULL;
	edit->selected = NULL;
	edit->mode = 0;
	edit->history = NULL;
	edit->heredoc = NULL;
	edit->sel_direction = LEFT;
	edit->has_returned = 0;
	g_edit = edit;
}

int				get_width(void)
{
	int			height;
	int			width;

	get_winsize(&width, &height);
	return (width);
}

static int		get_prompt_len(int quote)
{
	quote = QUOTE | DQUOTE | HEREDOC;
	if (quote & QUOTE)
		return (5);
	if (quote & DQUOTE)
		return (6);
	return (7);
}

int				get_line_number(void)
{
	int			ln_nb;
	int			n;
	t_line		*line;

	line = g_edit->first;
	ln_nb = 0;
	n = 3;
	if (line->line_type == STANDARD)
		n = (save_pwd(NULL) != NULL) ? (ft_strlen(save_pwd(NULL)) + n) : n + 1;
	else if (line->previous && line->previous->line_type != line->line_type)
		n += get_prompt_len(line->line_type);
	while (line->next != NULL)
	{
		if (line->data == '\n' || ((n / get_width()) > 0))
		{
			ln_nb++;
			n = 1;
		}
		else
			n++;
		line = line->next;
	}
	return (ln_nb);
}
