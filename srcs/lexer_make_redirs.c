/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer_make_redirs.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/10 22:43:30 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 17:25:55 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

static int		*find_fds(char *s, int kind)
{
	int	i;
	int	fd1;
	int	fd2;

	if (kind == TO_DLESS)
		return (ft_r(2, -1, -1));
	i = -1;
	fd1 = -1;
	fd2 = -1;
	while (s[++i])
	{
		if (ft_isdigit(s[i]))
		{
			if (fd1 == -1)
				fd1 = s[i] - 48;
			else if (fd2 == -1)
				fd2 = s[i] - 48;
			else
				return (ft_r(1, -2));
		}
	}
	return (ft_r(2, fd1, fd2));
}

static int		make_redirection(t_tlist *tl, char *input)
{
	t_redir	*redir;
	int		*fds;

	if (!(tl->next && tl->next->next))
		return (1);
	redir = (t_redir*)malloc(sizeof(t_redir));
	if (tl->next->t->kind != TO_GREAT_AND && tl->next->t->kind != TO_CLOSE_REDIR
			&& tl->next->t->kind != TO_DLESS)
		redir->file = ft_strdup(tl->next->next->t->value);
	else
		redir->file = NULL;
	redir->kind = tl->next->t->kind;
	redir->fd = -1;
	redir->heredoc = NULL;
	make_heredoc_redir(redir, input);
	ft_slistappn(&tl->t->redirs, redir);
	if ((fds = find_fds(tl->next->t->value, redir->kind))[0] == -2)
		return (1);
	redir->n1 = fds[0];
	redir->n2 = fds[1];
	ft_tlistdelone(&tl->next);
	if (redir->kind != TO_GREAT_AND && redir->kind != TO_CLOSE_REDIR)
		ft_tlistdelone(&tl->next);
	return (0);
}

t_redir			*find_heredoc(t_slist *redirs)
{
	while (redirs)
	{
		if (((t_redir*)redirs->data)->kind == TO_DLESS)
			return ((t_redir*)redirs->data);
		redirs = redirs->next;
	}
	return (NULL);
}

int				make_redirections(t_tlist **tokens, char *input)
{
	t_tlist		*iter;
	t_redir		*heredoc;

	iter = *tokens;
	while (iter)
	{
		if (iter->next && kind_isredir(iter->next->t->kind))
		{
			if (make_redirection(iter, input))
				return (1);
			if ((heredoc = find_heredoc(iter->t->redirs)))
				make_heredoc(tokens, iter, heredoc);
			continue ;
		}
		iter = iter->next;
	}
	return (0);
}
