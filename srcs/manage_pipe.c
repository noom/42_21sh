/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_pipe.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 14:05:50 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/21 11:39:03 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

int		read_pipe(int *fd)
{
	dup2(fd[0], STDIN_FILENO);
	close_pipe(fd);
	return (1);
}

int		write_pipe(int *fd)
{
	dup2(fd[1], STDOUT_FILENO);
	close_pipe(fd);
	return (0);
}

int		close_pipe(int *fd)
{
	close(fd[0]);
	close(fd[1]);
	return (0);
}

int		close_pipes(t_bst *ast)
{
	if (!ast)
		return (0);
	if (((t_tlist*)ast->data)->t->kind == TO_PIPE)
		close_pipe(((t_tlist*)ast->data)->t->pfd);
	close_pipes(ast->parent);
	return (0);
}

void	manage_pipe(t_bst *ast, char ***env, int flags)
{
	pipe(((t_tlist*)ast->data)->t->pfd);
	if ((((t_tlist*)ast->right->data)->t->pid = fork()) == -1)
		exit(1);
	else if (((t_tlist*)ast->right->data)->t->pid == 0)
	{
		flags = TO_READ | TO_DONT_FORK;
		if (ast->parent && ((t_tlist*)ast->parent->data)->t->kind == TO_PIPE)
			flags |= TO_WRITE_PARENT;
		fork_and_eval(ast->right, env, flags);
	}
	else
	{
		if (((t_tlist*)ast->left->data)->t->kind == TO_PIPE)
			manage_pipe(ast->left, env, TO_WRITE);
		else
			fork_and_eval(ast->left, env, TO_WRITE);
	}
}
