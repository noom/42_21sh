/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sighandlers.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/05 15:57:07 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 17:26:41 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

static void				sigint_handler(int sig)
{
	FT_UNUSED(sig);
	if (!g_edit->has_returned)
	{
		g_edit->cursor->cursor = 0;
		refresh_screen();
		print_line();
	}
	else
		return ;
	ft_putchar('\n');
	g_edit->cursor->cursor = 1;
	free_line();
	init_line();
	refresh_screen();
	print_line();
}

static void				sigstp_handler(int sig)
{
	struct termios		*orig_termios;
	char				cp[2];

	FT_UNUSED(sig);
	orig_termios = save_orig_termios(NULL);
	cp[0] = orig_termios->c_cc[VSUSP];
	cp[1] = '\0';
	restore_term();
	signal(SIGTSTP, SIG_DFL);
	ioctl(STDIN_FILENO, TIOCSTI, cp);
}

static void				sigcont_handler(int sig)
{
	t_caps				*caps;

	FT_UNUSED(sig);
	caps = save_caps(NULL);
	configure_term();
	signal(SIGTSTP, &sigstp_handler);
}

void					register_signals(void)
{
	signal(SIGTSTP, &sigstp_handler);
	signal(SIGCONT, &sigcont_handler);
	signal(SIGINT, &sigint_handler);
}
