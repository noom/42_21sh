/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 16:29:06 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 13:59:21 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

int		is_cpartofw(char c)
{
	return (ft_isalnum(c) || c == '-' || c == '-' || c == '~' || c == '!');
}

int		next_nonblank(char *s, int start)
{
	while (s[start++])
	{
		if (s[start] != ' ' && s[start] != '\t')
			return (start);
	}
	return (start);
}

char	*kind_tostr(int kind)
{
	if (kind == TO_UK)
		return ("UNKNOWN");
	if (kind == TO_WORD)
		return ("WORD");
	if (kind == TO_OP)
		return ("OPERATOR");
	if (kind == TO_CMD)
		return ("COMMAND");
	if (kind == TO_ARG)
		return ("ARGUMENT");
	if (kind == TO_GRP)
		return ("GROUP");
	if (kind == TO_PIPE)
		return ("PIPE");
	if (kind == TO_REDIR)
		return ("REDIRECTION");
	if (kind == TO_FILE)
		return ("FILE");
	else
		return ("0000");
}

int		kind_isop(int kind)
{
	if (kind == TO_OP
			|| kind == TO_PIPE
			|| kind_isredir(kind))
		return (1);
	return (0);
}

int		kind_isredir(int kind)
{
	if (kind == TO_REDIR
			|| kind == TO_LESS
			|| kind == TO_GREAT
			|| kind == TO_DLESS
			|| kind == TO_DGREAT
			|| kind == TO_LESS_AND
			|| kind == TO_GREAT_AND
			|| kind == TO_AND_GREAT
			|| kind == TO_AND_LESS)
		return (1);
	return (0);
}
