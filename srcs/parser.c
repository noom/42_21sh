/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 19:36:33 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 16:18:13 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

static t_bst	*make_node(t_tlist *tl, t_bst *parent, t_bst *left,
		t_bst *right)
{
	t_bst	*node;

	node = ft_bstnew(tl);
	node->parent = parent;
	node->left = left;
	node->right = right;
	return (node);
}

static t_bst	*make_pipe_node(t_tlist *last_pipe)
{
	t_bst	*node;
	t_tlist	*prev_pipe;

	if (!(last_pipe->prev && last_pipe->next))
		return (NULL);
	node = make_node(last_pipe, NULL, NULL, NULL);
	node->right = make_node(last_pipe->next, node, NULL, NULL);
	if ((prev_pipe = find_prev_pipe(last_pipe)))
	{
		node->left = make_pipe_node(prev_pipe);
		node->left->parent = node;
	}
	else
		node->left = make_node(last_pipe->prev, node, NULL, NULL);
	return (node);
}

t_tlist			*find_after_pipes(t_tlist *node)
{
	while (node)
	{
		if (node->t->kind == TO_SEMICOLON)
			return (node->next);
		node = node->next;
	}
	return (node);
}

t_bst			*make_ast(t_tlist *tl)
{
	t_bst	*node;
	t_tlist	*next_op;

	node = NULL;
	if (!tl)
		return (NULL);
	if (tl->t->kind == TO_SEMICOLON)
		return (make_ast(tl->next));
	if (((next_op = find_next_op(tl, (int[]){ TO_SEMICOLON, TO_PIPE })))
			&& next_op->t->kind == TO_PIPE)
		node = make_pipe_node(find_last_pipe(tl));
	if (!node)
	{
		node = make_node(tl, NULL, NULL, make_ast(tl->next));
		if (node->right)
			node->right->parent = node;
	}
	else
		make_normal_node(node, tl);
	return (node);
}

t_bst			*parse(t_tlist *tokens)
{
	t_bst	*ast;
	size_t	tokens_len;

	ast = NULL;
	tokens_len = ft_tlistlen(tokens);
	ast = make_ast(tokens);
	return (ast);
}
