/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history_move.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 16:44:37 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/22 13:57:42 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

t_line			*copy_history(t_line *hist)
{
	t_line		*new;

	refresh_screen();
	new = make_twin(hist);
	adjust_new_line(new);
	new->cursor = 1;
	free_current_level();
	g_edit->cursor = new;
	g_edit->first = new;
	move_cursor_to_tail();
	check_cursor();
	return (new);
}

t_line			*make_new_line(void)
{
	t_line		*new;

	refresh_screen();
	new = new_line();
	adjust_new_line(new);
	free_current_level();
	g_edit->cursor = new;
	g_edit->first = new;
	move_cursor_to_tail();
	return (new);
}

static void		tie_before_current(t_line *before)
{
	if (before != NULL)
	{
		before->next = g_edit->first;
		g_edit->first->previous = before;
	}
}

void			arrow_up(void)
{
	t_line		*before;
	t_line		*new;

	if (g_edit->history != NULL
		&& (g_edit->history->previous
		|| (!g_edit->history->previous && !g_edit->history->top_upped)))
	{
		move_cursor_to_tail();
		before = NULL;
		if (g_edit->first->previous != NULL)
		{
			before = g_edit->first->previous;
			before->next = NULL;
		}
		new = copy_history(g_edit->history->cmd);
		tie_before_current(before);
		if (g_edit->history->previous != NULL)
			g_edit->history = g_edit->history->previous;
		else
			g_edit->history->top_upped = 1;
		update_first();
		print_line();
	}
}

void			arrow_down(void)
{
	t_line		*before;
	t_line		*new;

	if (g_edit->history != NULL)
	{
		move_cursor_to_tail();
		before = NULL;
		g_edit->history->top_upped = 0;
		if (g_edit->first->previous != NULL)
		{
			before = g_edit->first->previous;
			before->next = NULL;
		}
		new = (g_edit->history->next)
			? (copy_history(g_edit->history->next->cmd)) : make_new_line();
		tie_before_current(before);
		if (g_edit->history->next != NULL)
			g_edit->history = g_edit->history->next;
		update_first();
		print_line();
	}
}
