/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_redirs.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 14:05:50 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 17:28:37 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void	manage_great(t_redir *redir, int *out)
{
	redir->fd = open(redir->file, O_CREAT | O_WRONLY | O_TRUNC, 0644);
	dup2(redir->fd, *out);
}

void	manage_dgreat(t_redir *redir, int *out)
{
	redir->fd = open(redir->file, O_APPEND | O_CREAT | O_RDWR, 0644);
	dup2(redir->fd, *out);
}

void	manage_andgreat(t_redir *redir)
{
	redir->fd = open(redir->file, O_CREAT | O_WRONLY | O_TRUNC, 0644);
	dup2(redir->fd, STDOUT_FILENO);
	dup2(redir->fd, STDERR_FILENO);
}

void	manage_redirs(t_slist *redirs)
{
	t_redir	*redir;
	int		out;

	while (redirs)
	{
		redir = (t_redir*)redirs->data;
		out = STDOUT_FILENO;
		if (redir->n1 != -1)
			out = redir->n1;
		if (redir->kind == TO_GREAT)
			manage_great(redir, &out);
		else if (redir->kind == TO_DGREAT)
			manage_dgreat(redir, &out);
		else if (redir->kind == TO_AND_GREAT)
			manage_andgreat(redir);
		else if (redir->kind == TO_GREAT_AND)
			dup2(redir->n1, redir->n2);
		else if (redir->kind == TO_LESS)
			for_less(redir);
		else if (redir->kind == TO_CLOSE_REDIR)
			close(redir->n1);
		redirs = redirs->next;
	}
}

int		close_redirs(t_slist *redirs)
{
	t_redir	*redir;

	while (redirs != NULL)
	{
		redir = (t_redir*)redirs->data;
		close(redir->fd);
		redirs = redirs->next;
	}
	return (0);
}
