/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_loop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 20:43:49 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 17:34:15 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

static void	clean_input(char *input)
{
	int	i;

	i = -1;
	while (input[++i])
	{
		if (input[i] == '\t')
			input[i] = ' ';
	}
}

static void	remove_bst(void **data)
{
	FT_UNUSED(data);
}

static void	exit_ctrld(char *line_result)
{
	if (ft_strcmp(line_result, "exit") == 0)
	{
		if (!g_edit->has_returned)
			free_line();
		exit_term();
	}
}

static int	free_when_error(char *line_result, t_tlist *tokens, t_bst *ast)
{
	free(line_result);
	ft_tlistdel(&tokens);
	ft_bstdel(&ast, &remove_bst);
	ft_putstr("syntax error\n");
	return (1);
}

int			loop(char ***env)
{
	int		running;
	char	*line_result;
	t_tlist	*tokens;
	t_bst	*ast;

	line_result = NULL;
	tokens = NULL;
	ast = NULL;
	running = 1;
	g_edit->has_returned = 0;
	if (!(line_result = edit_loop()))
		exit(1);
	exit_ctrld(line_result);
	clean_input(line_result);
	init_line();
	ft_putchar('\n');
	if ((tokens = lexe(line_result, ft_strlen(line_result))) == NULL)
		return (free_when_error(line_result, tokens, ast));
	ast = parse(tokens);
	if (interprete(ast, env))
		ft_put("error!\n");
	free(line_result);
	ft_tlistdel(&tokens);
	ft_bstdel(&ast, &remove_bst);
	return (running);
}
