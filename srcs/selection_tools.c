/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   selection_tools.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/25 12:03:26 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:14:02 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

int					enter_selection_mode(void)
{
	g_edit->mode = 1;
	refresh_screen();
	g_edit->sel_origin = g_edit->cursor;
	print_line();
	return (g_edit->mode);
}

void				undo_selection(void)
{
	t_line			*line;

	if ((line = g_edit->first) == NULL)
		return ;
	while (line != NULL)
	{
		line->selected = 0;
		line = line->next;
	}
}

void				free_selection(void)
{
	t_line			*damned;
	t_line			*line;

	if ((line = g_edit->selected) == NULL)
		return ;
	damned = line;
	while (line != NULL)
	{
		damned = line;
		line = line->next;
		free(damned);
	}
	g_edit->selected = NULL;
}

int					exit_selection_mode(void)
{
	refresh_screen();
	undo_selection();
	g_edit->mode = 0;
	g_edit->sel_origin = NULL;
	g_edit->sel_direction = 0;
	print_line();
	return (0);
}
