/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_basics.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 17:10:57 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:11:01 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

t_line					*create_line(char c)
{
	t_line				*line;

	if ((line = (t_line *)ft_memalloc(sizeof(t_line))) == NULL)
		return (NULL);
	line->data = c;
	line->previous = NULL;
	line->next = NULL;
	line->cursor = 0;
	line->selected = 0;
	line->ignored = 0;
	line->line_type = STANDARD;
	line->line_level = 0;
	return (line);
}

t_line					*new_line(void)
{
	t_line				*line;

	if ((line = create_line(' ')) == NULL)
		return (NULL);
	line->cursor = 1;
	line->selected = 0;
	line->ignored = 1;
	return (line);
}

void					add_to_line(char c)
{
	t_line				*new;
	t_line				*cursor;

	check_cursor();
	if ((new = create_line(c)) == NULL)
		return ;
	refresh_screen();
	new->line_level = g_edit->cursor->line_level;
	new->line_type = g_edit->cursor->line_type;
	cursor = g_edit->cursor;
	new->next = cursor;
	if (cursor->previous != NULL)
	{
		cursor->previous->next = new;
		new->previous = cursor->previous;
	}
	cursor->previous = new;
	update_first();
	print_line();
}

void					del_last_char(void)
{
	t_line				*damned;

	check_cursor();
	if (g_edit->cursor->previous == NULL
		|| g_edit->cursor->previous->line_level != g_edit->cursor->line_level)
		return ;
	refresh_screen();
	damned = g_edit->cursor->previous;
	if (damned->previous != NULL)
	{
		damned->previous->next = g_edit->cursor;
		g_edit->cursor->previous = damned->previous;
	}
	else
		g_edit->cursor->previous = NULL;
	free(damned);
	update_first();
	print_line();
}

char					check_closure(t_line *line)
{
	char				quote;

	quote = line->data;
	line = line->next;
	while (line && line->next)
	{
		if (line->data == '\n')
			return (0);
		if (line->data == quote)
			return (quote);
		line = line->next;
	}
	return (0);
}
