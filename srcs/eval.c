/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eval.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 14:05:50 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 18:09:21 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

static void	wait_procs(void)
{
	while (wait(NULL) >= 0)
		;
}

static int	manage_flags(t_bst *node, char ***env, int flags)
{
	int err;

	err = 0;
	if (flags & TO_READ)
		read_pipe(((t_tlist*)node->parent->data)->t->pfd);
	if (flags & TO_WRITE)
		write_pipe(((t_tlist*)node->parent->data)->t->pfd);
	if (flags & TO_WRITE_PARENT)
		write_pipe(((t_tlist*)node->parent->parent->data)->t->pfd);
	if (((t_tlist*)node->data)->t->redirs)
		manage_redirs(((t_tlist*)node->data)->t->redirs);
	close_pipes(node);
	if (eval_token(((t_tlist*)node->data)->t, env) == -1)
		err = -1;
	return (err);
}

int			fork_and_eval(t_bst *node, char ***env, int flags)
{
	int	err;

	err = 0;
	if (!(flags & TO_DONT_FORK))
	{
		if ((((t_tlist*)node->data)->t->pid = fork()) == -1)
			exit(1);
	}
	if (((t_tlist*)node->data)->t->pid == 0)
	{
		err = manage_flags(node, env, flags);
	}
	else if (!(flags & TO_DONT_FORK))
	{
		close_pipes(node);
		wait_procs();
		configure_term();
	}
	return (err);
}

int			package(t_token *t, char ***env)
{
	int		bi;
	char	*bin;

	if (t->args)
		update_input(t->args, *env);
	if ((bi = run_builtin(t, env)) > 0)
	{
		if (ft_strcmp(t->value, "cd") && ft_strcmp(t->value, "setenv")
				&& ft_strcmp(t->value, "unsetenv"))
			exit(0);
		return (1);
	}
	else if (ft_strchr(t->value, '/') != NULL)
	{
		if (test_access(t->value) == 0
				&& run_bin(t->value, t->args, *env) == -1)
			return (1);
	}
	else if ((bin = bin_path(*env, t->value)) != NULL)
	{
		if (test_access(bin) == 0 && run_bin(bin, t->args, *env) == -1)
			return (1);
		free(bin);
	}
	return (0);
}

int			eval_token(t_token *t, char ***env)
{
	if (t->args)
		update_input(t->args, *env);
	if (package(t, env))
		;
	else
	{
		ft_put("command '%s' not found\n", t->value);
		exit(1);
	}
	return (0);
}
