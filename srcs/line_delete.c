/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_delete.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 18:11:42 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/07/31 17:27:18 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void				del_next_char(void)
{
	t_line			*damned;
	t_line			*before;

	check_cursor();
	damned = g_edit->cursor;
	if (damned->next != NULL)
	{
		before = (g_edit->cursor->previous != NULL) ? damned->previous : NULL;
		damned->next->cursor = 1;
		g_edit->cursor = damned->next;
		refresh_screen();
		if (before)
		{
			g_edit->cursor->previous = before;
			before->next = g_edit->cursor;
		}
		else
			g_edit->cursor->previous = NULL;
		free(damned);
		update_first();
		reset_cursor();
		print_line();
	}
}

void				refresh_screen(void)
{
	t_caps			*caps;
	int				line_number;

	caps = save_caps(NULL);
	line_number = get_line_number();
	while (line_number > 0)
	{
		ft_putstr(caps->up);
		line_number--;
	}
	ft_putstr(caps->cr);
	ft_putstr(caps->cd);
}

int					exit_and_submit(void)
{
	char			*ligne;

	FT_UNUSED(ligne);
	refresh_screen();
	print_line_cursorless();
	add_to_history();
	free_heredoc();
	g_edit->has_returned = 1;
	return (0);
}

int					exit_without_submit(void)
{
	refresh_screen();
	print_line_cursorless();
	add_to_history();
	init_line();
	return (0);
}

int					get_conversion_length(void)
{
	int				len;
	t_line			*line;

	line = g_edit->head_list;
	len = 0;
	while (line != NULL)
	{
		if (line->ignored == 0)
			len++;
		line = line->next;
	}
	return (len);
}
