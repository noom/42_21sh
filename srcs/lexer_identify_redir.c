/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer_identify_redir.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/08 17:16:55 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 17:43:57 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

static int	*for_digit(char *s, int i)
{
	i++;
	if (s[i] == '<' || s[i] == '>')
	{
		i++;
		if (s[i] == '<' || s[i] == '>')
			return (ft_r(2, i, s[i - 1] == '<' ? TO_DLESS : TO_DGREAT));
		else if (s[i] == '&')
		{
			i++;
			if (ft_isdigit(s[i]))
				return (ft_r(2, i, s[i - 1] == '<' ? TO_LESS_AND
							: TO_GREAT_AND));
			else if (s[i] == '-')
				return (ft_r(2, i, TO_CLOSE_REDIR));
			else
				return (ft_r(2, i - 1, s[i - 1] == '<' ? TO_LESS_AND
							: TO_GREAT_AND));
		}
		else
			return (ft_r(2, i - 1, s[i - 1] == '<' ? TO_LESS : TO_GREAT));
	}
	else
		return (NULL);
}

static int	*for_lessmore(char *s, int i)
{
	i++;
	if (s[i] == '<' || s[i] == '>')
		return (ft_r(2, i, s[i - 1] == '<' ? TO_DLESS : TO_DGREAT));
	else
		return (ft_r(2, i - 1, s[i - 1] == '<' ? TO_LESS : TO_GREAT));
}

static int	*for_and(char *s, int i)
{
	i++;
	if (s[i] == '<' || s[i] == '>')
		return (ft_r(2, i, s[i - 1] == '<' ? TO_AND_LESS : TO_AND_GREAT));
	else
		return (NULL);
}

int			*end_of_redir(char *s, int start)
{
	if (ft_isdigit(s[start]))
		return (for_digit(s, start));
	else if (s[start] == '<' || s[start] == '>')
		return (for_lessmore(s, start));
	else if (s[start] == '&')
		return (for_and(s, start));
	else
		return (NULL);
}
