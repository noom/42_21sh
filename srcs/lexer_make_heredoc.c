/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer_make_heredoc.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: noom <echojnow@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/30 17:40:12 by noom              #+#    #+#             */
/*   Updated: 2018/07/31 17:27:36 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

static t_tlist	*make_pipe_token(void)
{
	t_token	*t;

	t = (t_token*)malloc(sizeof(t_token));
	t->args = NULL;
	t->kind = TO_PIPE;
	t->pid = 0;
	t->redirs = NULL;
	t->value = ft_strdup("|");
	return (ft_tlistnew(t));
}

static char		*get_heredoc(char *input)
{
	int	start;
	int	end;

	start = 0;
	while (input[start] && input[start] != '\n')
		start++;
	start++;
	end = ft_strlen(input) - 1;
	while (end >= 0 && input[end] != '\n')
		end--;
	return (end < start ? ft_strdup("")
			: ft_strsub(input, start, (end - start)));
}

static t_tlist	*make_echo_token(char *heredoc)
{
	t_token	*t;

	t = (t_token*)malloc(sizeof(t_token));
	t->args = (char**)malloc(sizeof(char*) * 2);
	t->args[0] = ft_strdup(heredoc);
	t->args[1] = NULL;
	t->kind = TO_GRP;
	t->pid = 0;
	t->redirs = NULL;
	t->value = ft_strdup("echo");
	return (ft_tlistnew(t));
}

void			make_heredoc_redir(t_redir *r, char *input)
{
	if (r->kind == TO_DLESS)
		r->heredoc = get_heredoc(input);
}

void			make_heredoc(t_tlist **tokens, t_tlist *tl, t_redir *r)
{
	t_tlist	*echo;
	t_tlist	*pipe;

	echo = make_echo_token(r->heredoc);
	pipe = make_pipe_token();
	echo->prev = tl->prev;
	if (!echo->prev)
		*tokens = echo;
	else
		echo->prev->next = echo;
	echo->next = pipe;
	pipe->prev = echo;
	pipe->next = tl;
	tl->prev = echo;
}
