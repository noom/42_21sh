/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   edit_debug.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 18:11:42 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 14:58:33 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void			ft_putstr_debug(char *str)
{
	int			len;
	int			i;

	if (str == NULL)
		return ;
	i = 0;
	len = ft_strlen(str);
	while (i < len - 1)
	{
		if (str[i] == ' ')
			ft_putstr(CHECK_COLOR);
		ft_putchar(str[i]);
		if (str[i] == ' ')
			ft_putstr(NO_COLOR);
		i++;
	}
	ft_putstr(CHECK_COLOR);
	ft_putchar(str[i]);
	ft_putstr(NO_COLOR);
}

void			print_vitefait(t_line *line)
{
	if (line == NULL)
		ft_putendl("LIGNE VIDE");
	while (line != NULL)
	{
		ft_putchar(line->data);
		line = line->next;
	}
}
