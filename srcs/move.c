/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 16:44:37 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/22 13:57:42 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void			move_right(void)
{
	t_line		*crs;

	check_cursor();
	crs = g_edit->cursor;
	if (crs->next != NULL && crs->next->line_level == crs->line_level)
	{
		if (g_edit->mode == 1 && crs->next->next != NULL)
		{
			if (crs == g_edit->sel_origin)
				g_edit->sel_direction = RIGHT;
			crs->selected = (g_edit->sel_direction == RIGHT) ? 1 : 0;
			crs->next->selected = (g_edit->sel_direction == RIGHT) ? 1 : 0;
		}
		crs->cursor = 0;
		if (crs->next->data != '\n')
			crs->next->cursor = 1;
		else if (crs->next->data == '\n' && crs->next->next != NULL)
			crs->next->next->cursor = 1;
		reset_cursor();
	}
	refresh_screen();
	print_line();
}

static void		switch_left(t_line *crs)
{
	if (crs == g_edit->sel_origin)
	{
		g_edit->sel_direction = LEFT;
		crs->selected =
			(crs->next != NULL && crs->next->selected == 1) ? 1 : 0;
	}
	else
		crs->selected = (g_edit->sel_direction == LEFT) ? 1 : 0;
	crs->previous->selected = (g_edit->sel_direction == LEFT) ? 1 : 0;
}

void			move_left(void)
{
	t_line		*crs;

	check_cursor();
	crs = g_edit->cursor;
	if (crs->previous != NULL && crs->previous->line_type == crs->line_type
	&& crs->previous->line_level == crs->line_level)
	{
		if (g_edit->mode == 1)
			switch_left(crs);
		if (crs->previous->data != '\n')
		{
			crs->cursor = 0;
			crs->previous->cursor = 1;
		}
		else if (crs->previous->data == '\n' && crs->previous->previous != NULL
				&& crs->previous->previous->line_level == crs->line_level)
		{
			crs->cursor = 0;
			crs->previous->previous->cursor = 1;
		}
		reset_cursor();
	}
	refresh_screen();
	print_line();
}

void			move_up(void)
{
	int			i;
	t_line		*crs;

	check_cursor();
	crs = g_edit->cursor;
	i = get_width();
	while (i > 0 && crs->previous != NULL
			&& crs->previous->line_type == crs->line_type
			&& crs->previous->line_level == crs->line_level)
	{
		if (g_edit->mode == 1)
			switch_left(crs);
		crs = crs->previous;
		i--;
	}
	g_edit->cursor->cursor = 0;
	crs->cursor = 1;
	reset_cursor();
	refresh_screen();
	print_line();
}

void			move_down(void)
{
	int			i;
	t_line		*crs;

	check_cursor();
	crs = g_edit->cursor;
	i = 0;
	while (i < get_width() && (crs->next != NULL)
			&& crs->next->line_type == crs->line_type
			&& crs->next->line_level == crs->line_level)
	{
		if (g_edit->mode == 1 && crs->next->next != NULL)
		{
			if (crs == g_edit->sel_origin)
				g_edit->sel_direction = RIGHT;
			crs->selected = (g_edit->sel_direction == RIGHT) ? 1 : 0;
			crs->next->selected = (g_edit->sel_direction == RIGHT) ? 1 : 0;
		}
		crs = crs->next;
		i++;
	}
	g_edit->cursor->cursor = 0;
	crs->cursor = 1;
	reset_cursor();
	refresh_screen();
	print_line();
}
