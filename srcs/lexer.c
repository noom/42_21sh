/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/08 17:16:55 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 17:43:57 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

static int		*end_of_value(char *s, int start)
{
	char	first;
	int		*result;

	first = s[start];
	if (first == '"' || first == '\'')
		return (ft_r(2, ft_first_among(s, (char[]){ '\n', '\'', '"', '\0' },
						start) + 1, TO_QUOTE));
	else if (first == '|')
		return (ft_r(2, start, TO_PIPE));
	else if (first == ';')
		return (ft_r(2, start, TO_SEMICOLON));
	else if ((result = end_of_redir(s, start)))
	{
		return (ft_r(2, result[0], result[1]));
	}
	else
	{
		result = (int[]){ ft_first_among(s,
				(char[]){ '\n', '\'', '"', '|', '>', '<', '&', ';', ' ', '\0' },
				start) };
		return (ft_r(2, result[0], TO_WORD));
	}
}

static t_token	*make_token(char *s, int start, int end, int kind)
{
	t_token	*t;

	t = NULL;
	if (kind == TO_QUOTE)
	{
		if (++start > --end)
			return (NULL);
		kind = TO_WORD;
	}
	if ((t = (t_token*)malloc(sizeof(t_token))) == NULL)
		return (NULL);
	t->value = ft_strsub(s, start, (end - start) + 1);
	t->kind = kind;
	t->args = NULL;
	t->pid = 0;
	t->redirs = NULL;
	return (t);
}

static t_tlist	*make_tokens(char *input, size_t len)
{
	t_tlist	*tokens;
	int		i;
	int		val_start;
	int		*result;
	t_token	*new_token;

	tokens = NULL;
	i = -1;
	val_start = -1;
	result = NULL;
	while (input[++i] && input[i] != '\n')
	{
		val_start = i;
		result = end_of_value(input, i);
		new_token = make_token(input, val_start, result[0], result[1]);
		if (new_token)
			ft_tlistappn(&tokens, new_token);
		if ((size_t)(i = next_nonblank(input, result[0]) - 1) >= len)
			break ;
	}
	return (tokens);
}

t_tlist			*lexe(char *input, size_t len)
{
	t_tlist	*tokens;

	tokens = make_tokens(input, len);
	ft_r(0);
	make_token_groups(&tokens);
	if (make_redirections(&tokens, input))
	{
		ft_tlistdel(&tokens);
		return (NULL);
	}
	if (!tokens_valid(tokens))
	{
		ft_tlistdel(&tokens);
		return (NULL);
	}
	return (tokens);
}
