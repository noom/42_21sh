/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   twentyonesh.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 20:43:49 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/31 17:34:15 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

int			init_terminal(struct termios *orig_termios, t_caps *caps)
{
	int				fd;

	fd = STDOUT_FILENO;
	if (isatty(STDOUT_FILENO))
		fd = open(ttyname(STDOUT_FILENO), O_RDWR);
	else
		return (ft_err(1, "ft_select: the terminal is not a tty"));
	save_orig_termios(orig_termios);
	save_caps(caps);
	save_fd(fd);
	tcgetattr(fd, orig_termios);
	register_signals();
	init_caps(caps);
	configure_term();
	return (0);
}

int			main(int argc, char **argv, char **envp)
{
	char			**env;
	char			*input;
	int				running;
	struct termios	orig_termios;
	t_caps			caps;

	if (envp[0] == NULL)
		exit(1);
	if ((env = init(envp)) == NULL)
		return (1);
	init_terminal(&orig_termios, &caps);
	FT_UNUSED(argc);
	FT_UNUSED(argv);
	input = NULL;
	running = 1;
	init_edit();
	while (running)
		running = loop(&env);
	exit_term();
	ft_ntsarr_free(&env);
	return (0);
}
