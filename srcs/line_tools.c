/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 17:02:58 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/24 20:06:41 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void					init_line(void)
{
	g_edit->first = new_line();
	g_edit->cursor = g_edit->first;
	g_edit->head_list = g_edit->first;
	g_edit->sel_direction = LEFT;
	reset_history();
}

int						combo_ctrl_d(void)
{
	if (get_whole_length() < 2)
	{
		refresh_screen();
		g_edit->cursor->cursor = 0;
		print_line();
		return (2);
	}
	else if (g_edit->cursor->next)
	{
		del_next_char();
	}
	return (1);
}

void					del_line(void)
{
	t_line				*current;
	t_line				*next;

	current = g_edit->first;
	while (current != NULL)
	{
		next = current->next;
		free(current);
		current = next;
	}
	g_edit->first = new_line();
	g_edit->cursor = g_edit->first;
}

char					*convert_line(void)
{
	char				*res;
	int					i;
	t_line				*line;
	int					len;

	line = g_edit->head_list;
	len = get_conversion_length();
	if ((res = (char *)malloc(sizeof(char) * (len + 1))) == NULL)
		return (NULL);
	i = 0;
	while (i < len && line != NULL)
	{
		if (line->ignored == 0)
		{
			res[i] = line->data;
			i++;
		}
		line = line->next;
	}
	res[i] = '\0';
	return (res);
}

void					clear_current_line(void)
{
	t_line				*before;
	t_line				*new;

	if (get_current_length() < 2)
		return ;
	if (g_edit->first->previous != NULL)
		before = g_edit->first->previous;
	new = new_line();
	adjust_new_line(new);
	new->cursor = 1;
	refresh_screen();
	free_current_level();
	g_edit->cursor = new;
	g_edit->first = new;
	check_cursor();
	update_first();
	print_line();
}
