/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/25 12:03:26 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:14:02 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

int					is_special_char(char c)
{
	if (c == '|' || c == ';' || c == '<' || c == '>')
		return (1);
	if (c == '\'' || c == '"' || c == '&')
		return (1);
	if (c == '\n' || c == ' ')
		return (1);
	return (0);
}

t_line				*copy_heredoc_stopper(t_line *line, char quote)
{
	t_line			*new;
	t_line			*head;

	head = NULL;
	if (line != NULL && !(is_special_char(line->data)))
		g_edit->heredoc = create_line(line->data);
	head = g_edit->heredoc;
	line = line->next;
	while (line != NULL && line->next != NULL && !(is_special_char(line->data)))
	{
		if (line->data == '\\' && quote == 0)
			line = line->next;
		new = create_line(line->data);
		g_edit->heredoc->next = new;
		new->previous = g_edit->heredoc;
		g_edit->heredoc = g_edit->heredoc->next;
		line = line->next;
	}
	return (head);
}

int					get_stopper(t_line *line)
{
	char			quote;

	if (line == NULL || (line && !line->next))
	{
		refresh_screen();
		print_line_cursorless();
		return (0);
	}
	while (line != NULL && line->data == ' ' && line->data != '\n')
		line = line->next;
	quote = 0;
	if (line != NULL && (line->data == '\'' || line->data == '"'))
	{
		if ((quote = check_closure(line)) == 0)
			return (1);
		else if (line->next->data == quote)
		{
			g_edit->heredoc = create_line('\0');
			return (1);
		}
		else
			line = line->next;
	}
	g_edit->heredoc = copy_heredoc_stopper(line, quote);
	return (1);
}

int					compare_to_stopper(void)
{
	t_line			*line;
	t_line			*stopper;

	line = g_edit->first;
	stopper = g_edit->heredoc;
	while (line != NULL && stopper != NULL)
	{
		if (line->data != stopper->data)
			return (0);
		line = line->next;
		stopper = stopper->next;
	}
	return (1);
}

t_line				*go_to_less_than(void)
{
	t_line			*line;

	line = g_edit->head_list;
	while (line != NULL && line->next != NULL)
	{
		if (line->data == '<' && line->next->data == '<')
			return (line->next->next);
		line = line->next;
	}
	return (NULL);
}
