/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history_tools.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/25 12:03:26 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:14:02 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void			free_current_level(void)
{
	t_line		*tmp;

	tmp = NULL;
	while (g_edit->first)
	{
		tmp = g_edit->first->next;
		free(g_edit->first);
		g_edit->first = tmp;
	}
	g_edit->first = NULL;
}

void			adjust_new_line(t_line *line)
{
	if (line == NULL)
		return ;
	while (line != NULL)
	{
		line->line_level += g_edit->cursor->line_level;
		line->line_type = g_edit->cursor->line_type;
		line = line->next;
	}
}

void			make_clean_line(void)
{
	t_line		*line;
	t_line		*damned;

	if (g_edit->cursor->line_type == HEREDOC && g_edit->cursor->previous
			&& g_edit->cursor->previous->data == '\n')
	{
		damned = g_edit->cursor->previous;
		g_edit->cursor->previous = damned->previous;
		damned->previous->next = g_edit->cursor;
		free(damned);
	}
	line = g_edit->head_list;
	while (line != NULL)
	{
		line->line_type = STANDARD;
		line->line_level = 0;
		line = line->next;
	}
	move_cursor_to_tail();
}

int				get_whole_length(void)
{
	int			len;
	t_line		*line;

	line = g_edit->head_list;
	len = 1;
	while (line->next != NULL)
	{
		len++;
		line = line->next;
	}
	return (len);
}

void			print_history_line(t_history *history)
{
	int			n;

	if (history->rank > 99)
		ft_putnbr(history->rank);
	else
	{
		ft_putnbr(history->rank);
		n = 4 - get_nblen(history->rank);
		while (n-- > 0)
			ft_putchar(' ');
	}
	ft_putchar(' ');
	print_vitefait(history->cmd);
	ft_putstr("\n");
}
