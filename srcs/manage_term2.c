/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_term2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 22:23:21 by echojnow          #+#    #+#             */
/*   Updated: 2018/06/25 17:30:32 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void						configure_term(void)
{
	struct termios			*orig_termios;
	t_caps					*caps;
	int						fd;
	struct termios			raw;

	orig_termios = save_orig_termios(NULL);
	caps = save_caps(NULL);
	fd = save_fd(-1);
	raw = *orig_termios;
	raw.c_lflag &= ~(ECHO | ICANON);
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
	ft_putstr_fd(caps->hic, fd);
}

void						restore_term(void)
{
	struct termios			*orig_termios;
	t_caps					*caps;
	int						fd;

	orig_termios = save_orig_termios(NULL);
	caps = save_caps(NULL);
	fd = save_fd(-1);
	ft_putstr_fd(caps->shc, fd);
	tcsetattr(STDIN_FILENO, TCSAFLUSH, orig_termios);
}

void						exit_term(void)
{
	int						fd;
	t_caps					*caps;

	fd = save_fd(-1);
	restore_term();
	free_when_exit();
	caps = save_caps(NULL);
	close(fd);
	ft_r(0);
	exit(0);
}
