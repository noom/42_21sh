/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/25 12:03:26 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:14:02 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void			print_quote_prompt(int c)
{
	ft_putstr(QUOTE_COLOR);
	if (c == QUOTE)
		ft_putstr("quote> ");
	else if (c == DQUOTE)
		ft_putstr("dquote> ");
	else
		ft_putstr("heredoc> ");
	ft_putstr(NO_COLOR);
}

void			print_correct_prompt(t_line *line)
{
	if (line->line_level == 0 && line->line_type == STANDARD)
	{
		ft_putstr(PROMPT_COLOR);
		if (save_pwd(NULL) != NULL)
		{
			ft_putstr(save_pwd(NULL));
			ft_putstr("> ");
		}
		else
			ft_putstr("$> ");
		ft_putstr(NO_COLOR);
	}
	else
		print_quote_prompt(line->line_type);
}

void			print_line_cursorless(void)
{
	t_line		*line;

	line = g_edit->first;
	print_correct_prompt(line);
	g_edit->cursor->cursor = 0;
	while (line != NULL)
	{
		if (line->ignored == 0)
		{
			print_char(line);
			if (line->data == '\n' && line->next != NULL
					&& line->line_type != line->next->line_type)
				print_quote_prompt(line->line_type);
		}
		line = line->next;
	}
}
