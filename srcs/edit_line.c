/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   edit_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 13:35:29 by echojnow          #+#    #+#             */
/*   Updated: 2018/06/25 17:00:32 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

static void			selection_mode(char *c)
{
	if (c[0] == TO_K_CTRL_P && c[1] == 0)
	{
		if (g_edit->mode == 0 && get_whole_length() > 1)
			g_edit->mode = enter_selection_mode();
		else if (g_edit->mode == 1)
			g_edit->mode = exit_selection_mode();
	}
	else if (c[0] == TO_K_CTRL_W && c[1] == 0)
	{
		if (g_edit->mode == 1)
			delete_selection();
		else
			paste_selection();
	}
	else if (c[0] == TO_K_CTRL_L && c[1] == 0)
	{
		if (g_edit->mode == 0)
			clear_current_line();
		else
			copy_selection();
	}
	else if (c[0] == TO_K_DEL && g_edit->mode == 0)
		del_last_char();
	else if (ft_is_printable(c[0]) || c[0] == ' ')
		add_to_line(c[0]);
}

static void			simple_moves(char *c)
{
	if ((!ft_strncmp(TO_K_CTRL_UP, c, 7)) && g_edit->cursor->previous)
		move_up();
	else if (!(ft_strncmp(TO_K_CTRL_DOWN, c, 7)) && g_edit->cursor->next)
		move_down();
	else if (!ft_strcmp(c, TO_K_UP) && g_edit->mode == 0)
		arrow_up();
	else if (!ft_strcmp(c, TO_K_DOWN) && g_edit->mode == 0)
		arrow_down();
	else if ((!ft_strcmp(c, TO_K_LEFT)) && g_edit->cursor->previous)
		move_left();
	else if ((!ft_strcmp(c, TO_K_RIGHT)) && g_edit->cursor->next)
		move_right();
	else if (!ft_strncmp(c, TO_K_SHIFT_LEFT, 7) && g_edit->mode == 0)
		go_to_previous_word();
	else if (!ft_strncmp(c, TO_K_SHIFT_RIGHT, 7) && g_edit->mode == 0)
		go_to_next_word();
	else if ((!ft_strcmp(c, TO_K_HOME)) && g_edit->cursor->previous
			&& g_edit->mode == 0)
		go_to_head();
	else if ((!ft_strcmp(c, TO_K_END)) && g_edit->cursor->next
			&& g_edit->mode == 0)
		go_to_tail();
	else
		selection_mode(c);
}

void				cut_moves(char *c)
{
	if (c[0] == TO_K_CTRL_U && c[1] == 0 && g_edit->cursor->previous
			&& g_edit->mode == 0)
		cut_left();
	else if (c[0] == TO_K_CTRL_K && c[1] == 0 && g_edit->cursor->next
			&& g_edit->mode == 0)
		cut_right();
	else if (!ft_strcmp(c, TO_K_DEL_FWD) && g_edit->mode == 0)
		del_next_char();
	else
		simple_moves(c);
}

static int			ret_moves_2(void)
{
	if (g_edit->cursor->line_type == HEREDOC && g_edit->heredoc != NULL
			&& compare_to_stopper())
		return (exit_and_submit());
	else if (!compare_to_stopper() || g_edit->heredoc == NULL)
		add_return_to_line(HEREDOC, 1);
	return (1);
}

int					ret_moves(void)
{
	int				return_eval;

	if (g_edit->cursor->line_type != HEREDOC)
	{
		if ((return_eval = eval_line()) == 0)
			return (exit_and_submit());
		else if (return_eval > HEREDOC)
		{
			if (!get_stopper(go_to_less_than()))
				return (0);
			return (add_return_to_line(return_eval - HEREDOC, 1));
		}
		else if (return_eval > 0)
		{
			if (return_eval == HEREDOC && !(g_edit->heredoc))
				if (!get_stopper(go_to_less_than()))
					return (0);
			return (add_return_to_line(return_eval, 1));
		}
	}
	else
		return (ret_moves_2());
	return (1);
}
