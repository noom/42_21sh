/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   edit_cut.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/25 12:03:26 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:14:02 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void					cut_left(void)
{
	t_line				*before;

	check_cursor();
	if (g_edit->cursor->previous == NULL)
		return ;
	before = (g_edit->first->previous) ? g_edit->first->previous : NULL;
	refresh_screen();
	g_edit->cursor->previous->next = NULL;
	if (before)
		before->next = g_edit->cursor;
	g_edit->cursor->previous = before;
	free_current_level();
	update_first();
	print_line();
}

void					cut_right(void)
{
	t_line				*damned;
	t_line				*before;

	check_cursor();
	if (g_edit->cursor->next == NULL)
		return ;
	before = NULL;
	refresh_screen();
	if (g_edit->cursor->previous != NULL)
		before = g_edit->cursor->previous;
	damned = g_edit->cursor;
	while (damned != NULL && damned->next != NULL)
	{
		damned = g_edit->cursor->next;
		free(g_edit->cursor);
		g_edit->cursor = damned;
	}
	if (before)
		before->next = g_edit->cursor;
	g_edit->cursor->previous = before;
	g_edit->cursor->cursor = 1;
	update_first();
	print_line();
}

void					copy_selection(void)
{
	t_line				*line;
	t_line				*head;

	check_cursor();
	if ((line = go_to_first_selected()) == NULL)
		return ;
	if (g_edit->selected != NULL)
		free_selection();
	g_edit->selected = create_line(line->data);
	head = g_edit->selected;
	if (line->next)
		line = line->next;
	refresh_screen();
	while (line && line->selected == 1)
	{
		g_edit->selected->next = create_line(line->data);
		g_edit->selected = g_edit->selected->next;
		line = line->next;
	}
	g_edit->selected = head;
	exit_selection_mode();
}

void					paste_selection(void)
{
	t_line				*copy;
	t_line				*before;

	check_cursor();
	if (g_edit->selected == NULL)
		return ;
	before = NULL;
	if (g_edit->cursor->previous != NULL)
		before = g_edit->cursor->previous;
	if ((copy = make_twin(g_edit->selected)) == NULL)
		return ;
	refresh_screen();
	if (before != NULL)
	{
		copy->previous = before;
		before->next = copy;
	}
	while (copy->next != NULL)
		copy = copy->next;
	copy->next = g_edit->cursor;
	g_edit->cursor->previous = copy;
	update_first();
	print_line();
}
