/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_update.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 16:52:27 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:56:53 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

void			update_first(void)
{
	t_line		*line;

	line = g_edit->cursor;
	while (line->previous != NULL
			&& line->previous->line_type == g_edit->cursor->line_type
			&& line->previous->line_level == g_edit->cursor->line_level)
		line = line->previous;
	g_edit->first = (line->data == '\n') ? line->next : line;
	while (line->previous != NULL)
		line = line->previous;
	g_edit->head_list = line;
}

void			update_selected(void)
{
	t_line		*line;

	line = g_edit->first;
	while (line != NULL && line->selected == 0)
		line = line->next;
	g_edit->selected = line;
}

int				get_current_length(void)
{
	int			len;
	t_line		*line;

	update_first();
	len = 0;
	if ((line = g_edit->first) == NULL)
		return (0);
	while (line)
	{
		len++;
		line = line->next;
	}
	return (len);
}

int				get_nblen(int n)
{
	int			len;

	len = 1;
	while (n / 10 > 0)
	{
		len++;
		n = n / 10;
	}
	return (len);
}
