/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quotes.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrasoloh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/25 12:03:26 by jrasoloh          #+#    #+#             */
/*   Updated: 2018/06/25 16:14:02 by jrasoloh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "twentyonesh.h"

static void				print_cursorless(void)
{
	move_cursor_to_tail();
	refresh_screen();
	print_line_cursorless();
	g_edit->cursor->cursor = 1;
}

static void				print_return(void)
{
	update_first();
	ft_putchar('\n');
	refresh_screen();
	print_line();
	reset_history();
}

int						add_return_to_line(int quote, int ret)
{
	t_line				*new;
	t_line				*before;

	check_cursor();
	move_cursor_to_tail();
	add_to_line(' ');
	g_edit->cursor->previous->ignored = 1;
	print_cursorless();
	if ((new = create_line('\n')) == NULL)
		return (0);
	before = (g_edit->cursor->previous) ? g_edit->cursor->previous : NULL;
	new->line_type = quote;
	new->line_level = g_edit->cursor->line_level + 1;
	g_edit->cursor->line_level++;
	g_edit->first = g_edit->cursor;
	g_edit->cursor->line_type = new->line_type;
	new->next = g_edit->cursor;
	if (before != NULL)
		before->next = new;
	new->previous = before;
	g_edit->cursor->previous = new;
	print_return();
	return (ret);
}

int						eval_line(void)
{
	t_line				*line;
	int					val;

	val = 0;
	if ((line = g_edit->head_list) == NULL)
		return (0);
	while (line != NULL && line->next != NULL)
	{
		if (line->data == '\\')
			line = line->next;
		else if (line->data == '<' && line->next->data == '<' && val == 0)
			val = HEREDOC;
		else if (line->data == '\'' && (val == 0 || val == HEREDOC))
			val += QUOTE;
		else if (line->data == '"' && (val == 0 || val == HEREDOC))
			val += DQUOTE;
		else if ((line->data == '\'' && (val == 1 || val == HEREDOC + 1))
				|| (line->data == '"' && (val == 2 || val == HEREDOC + 2)))
			val = (line->data == '\'') ? val - QUOTE : val - DQUOTE;
		line = line->next;
	}
	return (val);
}
